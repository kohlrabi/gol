#!/usr/bin/env python3

import pygame
import numpy
import scipy.signal

pygame.init()
clock = pygame.time.Clock()
pygame.display.set_caption("Game of Life")

grid = (1280,800)

screen = pygame.display.set_mode(grid)
pa = pygame.surfarray.pixels2d(screen)

gol_kernel = numpy.array([[1,1,1],[1,0,1],[1,1,1]])

def gol(m):
    r = scipy.signal.convolve2d(m, gol_kernel, 'same', 'wrap')
    return (r == 3) | (r + m == 3)

scale = 4
sgrid = (grid[0]//scale, grid[1]//scale)
seed = 0
numpy.random.seed(seed)
m = numpy.random.rand(*sgrid)<0.5

quit = False
i = 0
while not quit:
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key in (pygame.K_ESCAPE, pygame.K_q):
                quit = True

    if quit:
        break
    
    plm = m.repeat(scale,axis=0).repeat(scale,axis=1)

    pa[:] = (plm^True)*0xffffff
    
    if i % 6 == 0:
        m = gol(m)
    
    pygame.display.flip()
    i += 1 
    clock.tick(60)

